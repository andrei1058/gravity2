package com.andrei1058.gravity2;

import com.andrei1058.gravity2.command.GravityCommand;
import com.andrei1058.gravity2.configuration.GravityConfig;
import com.andrei1058.gravity2.configuration.MessageConfig;
import com.andrei1058.purplemc.PurpleMC;
import com.andrei1058.purplemc.arenamanager.ArenaManager;
import com.andrei1058.purplemc.server.ServerType;
import org.bukkit.plugin.java.JavaPlugin;

public class Gravity2Plugin extends JavaPlugin {

    private static ArenaManager arenaManager;
    private static Gravity2Plugin plugin;

    @Override
    public void onEnable() {
        plugin = this;

        PurpleMC.init(this, ServerType.MULTI_ARENA, null);
        arenaManager = ArenaManager.getInstance();

        GravityConfig.init();

        MessageConfig.setLanguage(new MessageConfig("en"));


        // register commands
        new GravityCommand("gravity").register();
    }

    public static ArenaManager getArenaManager() {
        return arenaManager;
    }

    public static Gravity2Plugin getPlugin() {
        return plugin;
    }
}
