package com.andrei1058.gravity2.language;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public interface ILanguage {

    String getMessage(Player player, String messagePath);

    String getMessage(CommandSender sender, String messagePath);
}
