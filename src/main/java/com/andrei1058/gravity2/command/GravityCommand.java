package com.andrei1058.gravity2.command;

import com.andrei1058.gravity2.configuration.MessageConfig;
import com.andrei1058.purplemc.arenamanager.ArenaManager;
import com.andrei1058.purplemc.server.SetupSession;
import com.andrei1058.spigot.commandlib.RootCommand;
import com.andrei1058.spigot.commandlib.SubCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class GravityCommand extends RootCommand {

    public GravityCommand(String name) {
        super(name);
        addSubCommand(new SubCommand("setup", new String[]{"setupArena"}, new String[]{"gravity.*", "gravity.setup"}, 0) {
            @Override
            public BaseComponent[] getListMsg(CommandSender commandSender) {
                return new ComponentBuilder(MessageConfig.get().getMessage(commandSender, "cmd.setup")).create();
            }

            @Override
            public void execute(String[] args, CommandSender s) {
                if (!(s instanceof Player)) {
                    s.sendMessage(ChatColor.RED + "This command is for players.");
                    return;
                }
                Player p = (Player) s;
                if (args.length != 1) {
                    p.sendMessage(MessageConfig.get().getMessage(p, "cmd.setup-usage"));
                    return;
                }
                if (SetupSession.isInSetupSession(p)){
                    p.spigot().sendMessage(new ComponentBuilder(MessageConfig.get().getMessage(p, "cmd.setup-p-in-session")).create());
                    return;
                }
                if (SetupSession.isInSetupSession(args[0])){
                    p.spigot().sendMessage(new ComponentBuilder(MessageConfig.get().getMessage(p, "cmd.setup-w-in-session")).create());
                    return;
                }
                SetupSession ss;
                try {
                    ss = new SetupSession(p, args[0]);
                } catch (SetupSession.SetupSessionException e) {
                    switch (e.getType()){
                        case UNKNOWN_WORLD:
                            p.spigot().sendMessage(new ComponentBuilder(MessageConfig.get().getMessage(p, "cmd.setup-no-map").replace("{name}", args[0])).create());
                            break;
                        case ARENA_NOT_DISABLED:
                            p.sendMessage("unhandled error type");
                            break;
                        case WORLD_ALREADY_LOADED:
                            p.spigot().sendMessage(new ComponentBuilder(MessageConfig.get().getMessage(p, "cmd.setup-w-in-session")).create());
                            break;
                        case WORLD_NAME_BAD_SYNTAX:
                            p.spigot().sendMessage(new ComponentBuilder(MessageConfig.get().getMessage(p, "cmd.setup-bad-syntax")).create());
                            break;
                    }
                    return;
                }
                ss.tpPlayer();
            }

            @Override
            public List<String> tabComplete(CommandSender commandSender, String[] strings) {
                return ArenaManager.getInstance().getMapAdapter().getWorldsInContainer();
            }
        });
    }

    @Override
    public BaseComponent[] getCmdHeader(CommandSender commandSender) {
        return new ComponentBuilder(MessageConfig.get().getMessage(commandSender, "cmd.header")).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Plugin by andrei1058").append(ChatColor.WHITE + "Powered by PurpleMC framework.").create())).create();
    }

    @Override
    public BaseComponent[] getDeniedMsg(CommandSender commandSender) {
        return new ComponentBuilder(MessageConfig.get().getMessage(commandSender, "cmd.denied")).create();
    }
}
