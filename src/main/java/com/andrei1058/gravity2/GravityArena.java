package com.andrei1058.gravity2;

import com.andrei1058.purplemc.arenamanager.ArenaEnableException;
import com.andrei1058.purplemc.arenamanager.EnableConditions;
import com.andrei1058.purplemc.arenamanager.PurpleArena;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

public class GravityArena extends PurpleArena {

    public GravityArena(String arenaName, EnableConditions enableConditions) throws ArenaEnableException {
        super(arenaName, enableConditions);
    }

    @Override
    public String getDisplayName(@Nullable Player player) {
        return getArenaName();
    }

    @Override
    public void checkGameEndConditions() {

    }

    @Override
    public PurpleArena loadClone() {
        return null;
    }

    @Nullable
    @Override
    public Location[] getLobbyRemoval() {
        return new Location[0];
    }
}
