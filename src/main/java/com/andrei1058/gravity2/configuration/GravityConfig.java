package com.andrei1058.gravity2.configuration;

import com.andrei1058.gravity2.Gravity2Plugin;

public class GravityConfig extends ConfigManager{

    private static GravityConfig config;

    private GravityConfig(){
        super(Gravity2Plugin.getPlugin(), "config", Gravity2Plugin.getPlugin().getDataFolder().getPath());
        config = this;
    }

    public static GravityConfig init(){
        new GravityConfig();
        return config;
    }

    public static GravityConfig get() {
        return config;
    }
}
