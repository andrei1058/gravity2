package com.andrei1058.gravity2.configuration;

import com.andrei1058.gravity2.Gravity2Plugin;
import com.andrei1058.gravity2.language.ILanguage;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Objects;

public class MessageConfig extends ConfigManager implements ILanguage {

    private static MessageConfig language;

    public MessageConfig(String name) {
        super(Gravity2Plugin.getPlugin(), name, Gravity2Plugin.getPlugin().getDataFolder().getPath() + "/messages");
        getYml().addDefault("cmd.prefix", "");
        getYml().addDefault("cmd.header", "&5Gravity2 commands:");
        getYml().addDefault("cmd.setup", "&5/gravity setup &fCreate or edit a level.");
        getYml().addDefault("cmd.setup-usage", "&8Usage: &5/gravity setup <mapName>");
        getYml().addDefault("cmd.setup-no-map", "&c{name} was not found in the worlds container.");
        getYml().addDefault("cmd.setup-p-in-session", "&cYou are already in a setup session!");
        getYml().addDefault("cmd.setup-w-in-session", "&cThis world is already being set-up by someone else.");
        getYml().addDefault("cmd.setup-bad-syntax", "&cThis world name has a bad syntax. Rename it in lowercase.");
        getYml().addDefault("cmd.denied", "{prefix}&cPermission denied or command not found.");
        getYml().options().copyDefaults(true);
        save();
    }

    @Override
    public String getMessage(Player player, String messagePath) {
        return ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(getYml().getString(messagePath)).replace("{prefix}", Objects.requireNonNull(getYml().getString("cmd.prefix"))));
    }

    @Override
    public String getMessage(CommandSender sender, String messagePath) {
        return ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(getYml().getString(messagePath)).replace("{prefix}", Objects.requireNonNull(getYml().getString("cmd.prefix"))));
    }

    public static ILanguage get(){
        return language;
    }

    public static void setLanguage(MessageConfig language) {
        MessageConfig.language = language;
    }
}
