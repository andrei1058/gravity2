package com.andrei1058.gravity2.configuration;

import com.andrei1058.gravity2.Gravity2Plugin;

public class LevelConfig extends ConfigManager {

    public LevelConfig(String name) {
        super(Gravity2Plugin.getPlugin(), name, Gravity2Plugin.getPlugin().getDataFolder().getPath());
    }
}
